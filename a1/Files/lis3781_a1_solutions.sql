-- a1
-- Backward-engineer the following query resultset: a.list (current)
-- jobtitleeach employee has, b.includename, c.address, d.phone,e.SSN, f.order by last name in descending order, g.use old-style join.
-- old-style join
select emp_id, emp_fname, emp_lname,
  CONCAT(emp_street, ",", emp_city, ",", emp_state, " ", substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
  CONCAT('(', substring(emp_phone,1,3),')',substring(emp_phone,4,3),'-', substring(emp_phone,7,4)) as phone_num,
  CONCAT(substring(emp_ssn,1,3),'-', substring(emp_ssn,4,2),'-', substring(emp_ssn,6,4)) as emp_ssn, job_title
  from job as j, employee as e
  where j.job_id = e.job_id
  order by emp_lname desc;

  -- 2) List all job titles and salaries each employee HAS and HAD, include employee ID, full name, job ID, job title, salaries, and respective
  -- dates, sort by employee ID and date, use old-style
  -- ANS
select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
    and eht_job_id = j.job_id
order by emp_id, eht_dates;
-- 3) List employee and dependent full names, DOBs, relationships, and age of both employee and respective dependents
-- sort by last name in ascending order, use natural join:
-- Note: see http://www.qcitr.com/dblinks.htm > calculating ages:
-- Answe: (Note: vertical space between emp_age and dep_age used only for legibility purposes)
SELECT emp_fname, emp_lname, emp_dob,
DATE_FORMAT(NOW(),'%Y') -
DATE_FORMAT(emp_dob,'%Y') -
DATE_FORMAT(NOW(),'00-%m-%d') <
DATE_FORMAT(emp_dob,'00-%m-%d') AS emp_age,

dep_fname, dep_lname, dep_relation, dep_dob,
DATE_FORMAT(NOW(),'%Y') -
DATE_FORMAT(dep_dob,'%Y') -
DATE_FORMAT(NOW(),'00-%m-%d') <
DATE_FORMAT(dep_dob,'00-%m-%d') AS dep_age

from employee
NATURAL JOIN dependent
order by emp_lname;

-- 4) Create a transaction that updates job ID 1 to the following title "owner", w/o the quotation marks, display
-- the job reords before and after the change, inside the trsaction
START transaction;
  select * from job;

  UPDATE job
  SET job_title='owner'
  WHERE job_id=1;
COMMIT;  