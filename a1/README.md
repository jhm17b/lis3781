# LIS3781 Advanced Database Management

## Jack H. Miller

### LIS3781 A1 Requirements:

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram and SQL code (Optional)
5. Bitbucket Repo Link

## A1 Business Rules:

The human resource (HR) department of the ACME company wants to contract a database 
modeler/designer to collect the following employee data for tax purposes: job description, length of 
employment, benefits, number of dependents and their relationships, DOB of both the employee and any 
respective dependents. In addition, employees’ histories must be tracked. Also, include the following 
business rules: 

- Each employee may have one or more dependents. 
- Each employee has only one job.  
- Each job can be held by many employees. 
- Many employees may receive many benefits. 
- Many benefits may be selected by many employees (though, while they may not select any benefits—
any dependents of employees may be on an employee’s plan). 

## Screenshots

### AMPPS Installation
![AMPPS.PNG](imgs/AMPPS.png)
### A1 ERD
![ERDa1.PNG](imgs/ERDa1.PNG)
### SQL Statements
- 1) Backward-engineer the following query resultset: a.list (current)
- jobtitleeach employee has, b.includename, c.address, d.phone,e.SSN, f.order by last name in descending order, g.use old-style join.
- old-style join
![Q1.png](imgs/Q1.JPG)
- 2) List all job titles and salaries each employee HAS and HAD, include employee ID, full name, job ID, job title, salaries, and respective
- dates, sort by employee ID and date, use old-style
- ANS
![Q2.png](imgs/Q2.JPG)
- 3) List employee and dependent full names, DOBs, relationships, and age of both employee and respective dependents
- sort by last name in ascending order, use natural join:
- Note: see http://www.qcitr.com/dblinks.htm > calculating ages:
- Answe: (Note: vertical space between emp_age and dep_age used only for legibility purposes)
![Q3.png](imgs/Q3.JPG)
- 4) Create a transaction that updates job ID 1 to the following title "owner", w/o the quotation marks, display
- the job reords before and after the change, inside the trsaction
![Q4.png](imgs/Q4.JPG)
- 5) Create a stored procedure that adds one record to the benefit table with the following values: benefit name “new benefit,” benefit notes “testing,” both attribute values w/o the quotation marks, display the benefit 
- records before and after the change, inside the stored procedure: 
![Q5.png](imgs/Q5.JPG)
- 6) List employees’ and dependents’ names and social security numbers, also include employees’ e-mail addresses, dependents’ mailing addresses, and dependents’ phone numbers. *MUST* display *ALL* employee data, 
- even where there are no associated dependent values. (Major table: all rows displayed, minor table: display null values.) 
![Q6.png](imgs/Q6.JPG)
- 7) Create “after insert on employee” trigger that automatically creates an audit record in the emp_hist table. 
![Q7.png](imgs/Q7.JPG)

## git commands
--git init
Initializes git repos for a new/existing project

--git clone
To copy a git repos from remote source

--git status
Checks the status of your files that you have change in the working directory

--git add
adds changes to stage or index in your working directory

--git commit
commits your changes and sets it to new commit object for your remote

--git push/git pull
push or pull your changes to remote

https://bitbucket.org/jhm17b/bitbucketstationlocations/src/master/
