# LIS3781

## Jack H. Miller
***************************************
### LIS3781 a5 Business Rules:  
Expanding upon the high-volume home office supply company’s data tracking requirements, the CFO 
requests your services again to extend the data model’s functionality. The CFO has read about the 
capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a 
smaller data mart as a test platform. He is under pressure from the members of the company’s board of 
directors who want to review more detailed sales reports based upon the following measurements: 
1. Product 
2. Customer 
3. Sales representative 
4. Time (year, quarter, month, week, day, time) 
5. Location
********************************************
### LIS3781 a5 Requirements: 
- ERD Screenshot
- Table Creation and Insert Screenshots
- SQL Report Solution Example Screenshot
### ERD Screenshot
![ERD Screenshot](imgs/FullERD.jpg)
![ERD Screenshot](imgs/A5ERD.JPG)
![ERD Screenshot](imgs/A5ERD2.JPG)
***************************************
### Created Tables
![Created Tables Screenshot](imgs/A5Tables.JPG)
****************************************
### Populated Tables
![Store Table Screenshot](imgs/a5StoreTable.JPG)
![Time Table Screenshot](imgs/a5TimeTable.JPG)
*****************************************
### SQL Report Solution Example
![#4SQLReportSolution](imgs/sqlreport4answer.JPG)
