# LIS3781 Advanced Database Management
## Jack H. Miller
******************************************************************************
### Project 2 Overview
- Screenshot example of MongoDB shell command
- Screenshot example of at least one required report, and JSON code solution
******************************************************************************
### MongoDB shell command Screenshot
#### Show Collections
![Mongodb Shell Command](img/showcollections.JPG)
******************************************************************************
## MongoDB report solution command Screenshot
### Find Commands
![Mongodb report solution](img/findcommand.JPG)
#### find and limit Command
![Mongodb report solution](img/findlimit5.JPG)

#### find "Brooklyn" "Borough" and limit Command
![Mongodb report solution](img/findspecificlimit2.JPG)
#### find and count Command
![Mongodb report solution](img/findcountcommand.JPG)
#### find Command (Restaurant id)
![Mongodb report solution](img/FindRestid.JPG)
#### find Command (Restaurant by zip)
![Mongodb report solution](img/FindZip.JPG)
******************************************************************************

Insert Command (Raccoon Cafe)            |  Insert Confirmation Command (Raccoon Cafe)
:-------------------------:|:-------------------------:
![Mongodb report solution](img/MongoInsert.JPG)  |  ![Mongodb report solution](img/InsertConfirm.JPG)




