# LIS3781 Advanced Database Management
### DB Engines Utilized:
- MySQL
- Oracle
- MS SQL Server
- MongoDB
## Jack H. Miller
**********************************************************
### LIS3781 Requirements:
*Course Work links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - AMPPS Installation
    - Questions
    - Entity Relationship Diagram and SQL code (Optional)
    - Bitbucket Repo Link
**********************************************************
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Company and Customer Table Statements
    - Insert Statements
    - Query Results for populated tables
**********************************************************
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - SQL Code used to create tables
    - SQL Code used to populate tables
    - Query Results for populated tables
**********************************************************
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - ERD (Tables must be populated using remote labs in MS Server)
    - SQL Statement Questions
    - Populated Persons table (Optional)
**********************************************************
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - ERD Screenshot
    - Optional: SQL code for the required reports
    - Table Creation and Population Screenshots
**********************************************************
6. [P1 README.md](p1/README.md "Court Case Database Project 1 README.md file")
    - Screenshot of ERD
    - Screenshot of populated Persons Table
    - SQL code Example for required report
**********************************************************
7. [P2 README.md](p2/README.md "My Project 2 README.md file")
    - Screenshot example of MongoDB shell command
    - Screenshot example of at least one required report, and JSON code solution
    