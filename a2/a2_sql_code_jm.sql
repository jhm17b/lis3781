drop database if exists jhm17b;
create database if not exists jhm17b;
use jhm17b;

-- ===========================
--        Company Table
-- ===========================

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
	cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-CORP','S-CORP','NON-PROFIT-CORP','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip int(9) unsigned zerofill NOT NULL COMMENT 'no dashes',
    cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero filled, but not US area codes',
    cmp_ytd_sales DECIMAL (10,2) unsigned NOT NULL COMMENT '12,345,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-CORP','507 - 20th Ave. E. Apt.2A','Seattle','WA','081226749','2065558080','12345678.00',null,'https://www.technologies.ci.fsu.edu/node/72','company notes1'),
(null,'S-CORP','908 West Capital Way','Tacoma','WA','004011298','2065557070','995600.00',null,'http://www.MillerWare.com','company notes 2'),
(null,'Non-Profit-Corp','300 W. Way Street','Dayton','CA','004011200','3005006767','900700.50',null,'http://www.fullfuntech.com','company notes 3'),
(null, 'LLC','4110 Oldman way Rd.','Richmond','VA','0003042034','4003334567','880750.50',null,'http://www.gizmotech.com','company notes 4'),
(null,'Partnership','3000 Greenway St.','Farsfield','CA','004011203','3400506060','990500.50',null,'http://www.farsfieldtech.com','company notes 5');

SHOW WARNINGS;

-- ===========================
--        Customer Table
-- ===========================

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
	cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn BINARY(64) NOT NULL,
    cus_salt BINARY(64) NOT NULL COMMENT '*only* demo purposes - do not use *salt* in the name!',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip int(9) unsigned zerofill NULL,
    cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero filled, but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) unsigned NULL COMMENt '12,345.67',
    cus_tot_sales DECIMAL(7,2) unsigned NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id),
    
    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),
    
    -- Comment constraint line to demo DBMS auto value when *not* using "constraint" option for foreign keys, then...
    -- SHOW CREATE TABLE customer;
    
    CONSTRAINT fk_customer_company
      FOREIGN KEY (cmp_id)
      REFERENCES company (cmp_id)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- Salting and hashing sensitive data(E.G, SSN). Normally, each record would recieve unique random salt!
set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 00456789),512)),@salt,'Discount','Jack','Miller','300 Westeria Way','Sarasota','FL','00065050','9412223030','Jack@gmail.com','500.50','100','customer notes 1'),
(null,2,unhex(SHA2(CONCAT(@salt, 00456789),512)),@salt,'Impulse','Jake','Brown','300 Main St.','Tampa','FL','00070050','9412004030','Jake@gmail.com','750.50','250','customer notes 2'),
(null,2,unhex(SHA2(CONCAT(@salt, 00456789),512)),@salt,'Loyal','Sarah','Fullman','1020 Main St.','El Paso','TA','00980070','8004504030','Sarah@gmail.com','8000.50','350','customer notes 3'),
(null,2,unhex(SHA2(CONCAT(@salt, 00456789),512)),@salt,'Wandering','Steve','Yeller','3001 Second St.','Sarasota','FL','01230056','9413405030','Steve@gmail.com','7500.50','2500','customer notes 4'),
(null,2,unhex(SHA2(CONCAT(@salt, 00456789),512)),@salt,'Discount','Sasha','Jaegar','501 Main St.','Tampa','FL','00070234','9415504012','Sasha@gmail.com','120.50','3000','customer notes 5');

SHOW WARNINGS;
-- set foreign_key_checks=1;

select * from company;
select * from customer;

-- 1. Limit User1 to select, update, and delete privleiges on company and customer tables
-- ========================================================================================
--Two step process:
--Create new user with CREATE USER statement, then use GRANT statement:
--1) CREATE USER IF NOT EXISTS 'username@localhost' IDENTIFIED BY 'password';
--2) GRANT select, update, delete, on databasename.company to 'username@localhost';
-- ========================================================================================

--First: create users: Note (ONLY) creates user, doesn't grant any persmissions!
CREATE USER IF NOT EXISTS 'user1@localhost' IDENTIFIED BY 'test1';
CREATE USER IF NOT EXISTS 'user2@localhost' IDENTIFIED BY 'test2';
flush privileges;
--NOTE: (MUST) flush privileges after creating or removing users and/or privilegs!

-- Check:
use mysql;
select * from user;

--also, can drop users:
drop user if exists someuser;
flush privileges;

--Next: grant permissions:
GRANT select, update, delete
  on jhm17b.company
  to user1@"localhost";
  show warnings;

GRANT select, update, delete
  on jhm17b.customer
  to user1@"localhost";
  show warnings;

--OR:
GRANT select, update, delete
  on jhm17b.*
  to user1@"localhost";
  show warnings;

--2. Limit user2 to select, and insert privileges on customer table
GRANT select, insert
   on jhm17b.customer
   to user2@"localhost";
   show warnings;

FLUSH PRIVILEGES;

--3. Show grants for you, user1, user2 (must log into each user!)
--a. yours/admin: show grants;
--b. user1 (logged in as user 1): show grants;
--c. user 2 (logged in as admin): show grants for 'user2'@'localhost';

--Note: Can show individual privileges as admin:
--Example: SHOW GRANTS FOR 'user1'@'localhost';

--4. Display current user2 (logged in as user2) and MySQL version
select user(), version();

--*Be sure* to return to *your* database!
use jhm17b;

--5. list tables (as admin)
show tables;

--6. Display structures for both tables (as admin)
--6a.
describe company;

--6b.
describe customer;

--7. Display data for both tables


