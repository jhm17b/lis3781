# LIS3781 Advanced Database Management
## Jack H. Miller
**********************************************************************************************************************************************
### LIS3781 A2 Requirements:
- Table and Insert Statements
- Query Results for populated tables
## Screenshots:
***********************************************************************************************************************************************
### Company Table Statement
![Company Insert Statement](imgs/CompanyInsertSQLCode.JPG)
*********************************************************************************************************************************************
### Comapany Insert Values
![Comapany Insert Values](imgs/CompanyInsertValues.JPG)
*********************************************************************************************************************************************
### Company populated Table Query Results
![Comapany Populated Tables](imgs/Companytable.JPG)

*********************************************************************************************************************************************
### ====================================================================
### Customer Table Statement
![CustomerInsertSQLCode.jpg](imgs/CustomerInsertSQLCode.JPG)
![CustomerInsertSQLCode2.jpg](imgs/CustomerInsertSQLCode2.JPG)
*********************************************************************************************************************************************
### Customer Insert Values
![Customer Insert Values.jpg](imgs/CustomerInsertValue.JPG)
*********************************************************************************************************************************************
### Customer populated Table Query Results
![Customer Populated Tables](imgs/CustomerTable.JPG)