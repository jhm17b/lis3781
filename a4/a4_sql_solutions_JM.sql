SET ANSI_WARNINGS ON;
GO

-- avoids error that user kept db connection open
use master;
GO

-- drop existing database if exists

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
DROP DATABASE jhm17b;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
CREATE DATABASE jhm17b;
GO

use jhm17b;
GO

-- -------------------------------------------------------
--                  Person Table Creation               --
-- -------------------------------------------------------
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO 

CREATE TABLE dbo.person
(
    per_id SMALLINT not null identity(1,1),
    per_ssn binary(64) NULL,
    per_salt binary(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip int NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c','s')),
    per_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    --make sure SSNs and State Ids are unique
    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

-- -------------------------------------------------------------------------
-- Phone Table
-- -----------------------------------------------
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    phn_num BIGINT NOT NULL check (phn_num like'[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) NOT NULL CHECK (phn_type IN('h', 'c', 'w', 'f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY(phn_id),

    CONSTRAINT fk_phone_person
      FOREIGN KEY (per_id)
      REFERENCES dbo.person (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ---------------------------------------------
-- Customer Table
-- ---------------------------------------------
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT NOT NULL,
    cus_balance decimal(7,2) NOT NULL check (cus_balance >= 0),
    cus_total_sales decimal(7,2) NOT NULL check (cus_total_sales >= 0),
    cus_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
      FOREIGN KEY (per_id)
      REFERENCES dbo.person (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ---------------------------------------------
-- slsrep (sales rep) table
-- ---------------------------------------------
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT NOT NULL,
    srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) NOT NULL check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) NOT NULL check (srp_ytd_comm >= 0),
    srp_notes VARCHAR(45) NULL,
    PRIMARY KEY(per_id),

    CONSTRAINT fk_slsrep_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ------------------------------------------------
--  Table srp_hist
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    sht_type char(1) not null check (sht_type IN('i', 'u', 'd')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) NOT NULL check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) NOT NULL check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) NOT NULL check (sht_yr_total_comm >= 0),
    sht_notes VARCHAR(45) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
      FOREIGN KEY (per_id)
      REFERENCES dbo.slsrep (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Contact Table
-- -----------------------------------------------------
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id INT NOT NULL identity(1,1),
    per_cid SMALLINT NOT NULL,
    per_sid SMALLINT NOT NULL,
    cnt_date datetime NOT NULL,
    cnt_notes VARCHAR(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
      FOREIGN KEY (per_cid)
      REFERENCES dbo.customer (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

-- SEE NOTE BELOW
    CONSTRAINT fk_contact_slsrep
      FOREIGN KEY (per_sid)
      REFERENCES dbo.slsrep (per_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);
-- NOTE: Cannot have cascade paths on both fks in CONTACT, because records in CONTACT tables are linked w/ customer records
-- AND records in contact table are linked with SLSREP records. Even if records in contact table never belonged to both customer and slsrep
-- it would be impossible to make CONTACT tables' records cascade delete for both customer and slsrep tables
-- because there are multiple cascading paths from person to contact table cannot do both at same time


-- ------------------------------------------------------------
-- table [order]
-- must use delimeter [] for reserved words (e.g order)
-- -----------------------------------------------------------
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id INT NOT NULL identity(1,1),
    cnt_id INT NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255),
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
      FOREIGN KEY (cnt_id)
      REFERENCES dbo.contact (cnt_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- --------------------------------------------------------
-- Store table
-- ---------------------------------------------------------
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
   str_id SMALLINT NOT NULL identity(1,1),
   str_name VARCHAR(45) NOT NULL,
   str_street VARCHAR(30) NOT NULL,
   str_city VARCHAR(30) NOT NULL,
   str_state CHAR(2) NOT NULL DEFAULT 'FL',
   str_zip INT NOT NULL check (str_zip like'[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
   str_phone BIGINT NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
   str_email VARCHAR(100) NOT NULL,
   str_url VARCHAR(100) NOT NULL,
   str_notes VARCHAR(255) NULL,
   PRIMARY KEY (str_id)
);

-- ---------------------------------------------------------
-- Invoice Table
-- ---------------------------------------------------------
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
    inv_id INT NOT NULL identity(1,1),
    ord_id INT NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL check (inv_total >= 0),
    inv_paid INT NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

-- create 1:1 relationship with order by making ord_id unique
CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

CONSTRAINT fk_invoice_order
  FOREIGN KEY (ord_id)
  REFERENCES dbo.[order] (ord_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,

CONSTRAINT fk_invoice_store
  FOREIGN KEY (str_id)
  REFERENCES dbo.store (str_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

-- -----------------------------------------------------------
-- Payment Table
-- -----------------------------------------------------------
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
    pay_id INT NOT NULL identity(1,1),
    inv_id INT NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL check (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
      FOREIGN KEY (inv_id)
      REFERENCES dbo.invoice (inv_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------
-- Vendor Table
-- ----------------------------------------------------------
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
    ven_id SMALLINT NOT NULL identity(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip INT NOT NULL check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone BIGINT NOT NULL check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL,
    ven_url VARCHAR(100) NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);

-- ------------------------------------------------------------
-- Product Table
-- -----------------------------------------------------------
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL identity(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL check (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL check (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL check (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL check (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
      FOREIGN KEY (ven_id)
      REFERENCES dbo.vendor (ven_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------
-- Product_hist Table
-- ----------------------------------------------------------
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
    pht_id INT NOT NULL identity(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL check (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL check (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
      FOREIGN KEY (pro_id)
      REFERENCES dbo.product (pro_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);
-- ----------------------------------------------------
-- Order_line Table
-- ----------------------------------------------------
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oln_id INT NOT NULL identity(1,1),
    ord_id INT NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL check (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL check (oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

-- must use delimeters [] on reserved words
    CONSTRAINT fk_order_line_order
      FOREIGN KEY (ord_id)
      REFERENCES dbo.[order] (ord_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
      FOREIGN KEY (pro_id)
      REFERENCES dbo.product (pro_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

select * from information_schema.tables;

-- converts to binary
SELECT HASHBYTES('SHA2_512', 'test');

-- length 64 bytes
SELECT len(HASHBYTES('SHA2_512', 'test'));













-- ----------------------------------------------------------------------------------------------------------------------------- --
-- Data for person table: 5 sales reps: 1-5, 10 customers: 6-15                                                                  --
-- NOTE: do NOT include attribute name or value for autoincrement attributes (i.e pks)                                           --
-- MUST use inital placedholder values for per_SSN, because of unique constraint. Qill be replaced by stored procedure values!   --
-- ----------------------------------------------------------------------------------------------------------------------------- --
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@gmail.com', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1979-11-03', '100 Moutain Pass', 'Gotham', 'IL', 300402222, 'bwayne@gmail.com', 's', NULL),
(3, NULL, 'Bucky', 'Barns', 'm', '1921-08-02', '400 Southern Drive', 'Rochester', 'NY', 324402220, 'bbarns@gmail.com', 's', NULL),
(4, NULL, 'Peter', 'Parker', 'm', '1999-11-03', '300 North Ave', 'Queens', 'NY', 324402200, 'pparker@gmail.com', 's', NULL),
(5, NULL, 'Tony', 'Stark', 'm', '1969-09-05', '437 Southern Drive', 'Manhatten', 'NY', 324402234, 'tstark@gmail.com', 's', NULL),
(6, NULL, 'Bruce', 'Banner', 'm', '1977-11-07', '300 Northern Drive', 'Rochester', 'NY', 324402201, 'bbanner@gmail.com', 'c', NULL),
(7, NULL, 'Mathew', 'Murdock', 'm', '1994-10-09', '437 Main Drive', 'Hells Kitchen', 'NY', 324402244, 'mmurdock@gmail.com', 'c', NULL),
(8, NULL, 'Clint', 'Bannon', 'm', '1980-09-03', '200 Second Street', 'Manhatten', 'NY', 324402221, 'cbannon@gmail.com', 'c', NULL),
(9, NULL, 'Hank', 'Pym', 'm', '1945-10-03', '400 Siesta Drive', 'Los Angeles', 'CA', 526602222, 'hpym@gmail.com', 'c', NULL),
(10, NULL, 'Star', 'Lord', 'm', '1989-12-03', '999 Somewhere Drive', 'Rochester', 'NY', 324409999, 'slord@gmail.com', 'c', NULL),
(11, NULL, 'Franky', 'Stars', 'm', '1977-12-05', '1230 Buena Vista', 'Sarasota', 'FL', 323304040, 'fstars@gmail.com', 'c', NULL),
(12, NULL, 'Goop', 'Goopers', 'm', '1999-09-09', '4040 Flash Drive', 'Tampa', 'FL', 325503030, 'ggoopers@gmail.com', 'c', NULL),
(13, NULL, 'Jack', 'Miller', 'm', '1999-04-03', '437 Southern Ways', 'Sarasota', 'FL', 324404040, 'jmiller@gmail.com', 'c', NULL),
(14, NULL, 'Gamora', 'Titan', 'm', '1900-10-03', '999 Somewhere Space', 'Tampa', 'FL', 324406060, 'gtitan@gmail.com', 'c', NULL),
(15, NULL, 'Rocket', 'Racoon', 'm', '2009-10-03', '999 Flash Drive', 'Tampa', 'FL', 324401111, 'rracoon@gmail.com', 'c', NULL);
GO

-- --------------------------------------------------------------------------
--  Data for slsrep (sales rep) table
-- -----------------------------------------------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 800000, 35000, 3500, NULL),
(3, 1500000, 84000, 9650, 'RADICAL Salesman'),
(4, 125000, 87000, 15300, NULL),
(5, 980000, 43000, 8750, NULL);

select * from dbo.slsrep;

-- -----------------------------------------------------------
-- Data for Customer table
-- -----------------------------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 14789, NULL),
(8, 0, 14789, 'customer always pays on time'),
(9, 981.73, 14789, 'high balance'),
(10, 541.23, 14789, NULL),
(11, 251.02, 14789, 'good customer'),
(12, 582.67, 14789, 'Previously paid in full'),
(13, 121.67, 14789, 'Recent customer'),
(14, 765.43, 14789, 'Buys bulk'),
(15, 304.39, 456.81, 'has not purchased recently');

select * from dbo.customer;

-- ----------------------------------------------------------------
-- Data for contact table
-- -----------------------------------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-01-01', NULL),
(3, 7, '2002-09-01', NULL),
(2, 7, '2003-07-01', NULL),
(4, 7, '2004-06-01', NULL),
(5, 8, '2003-06-01', NULL),
(4, 8, '1999-09-01', NULL),
(1, 9, '1999-02-01', NULL),
(5, 9, '1999-01-01', NULL),
(3, 11, '2005-03-01', NULL),
(4, 13, '2005-01-01', NULL),
(2, 15, '2005-01-01', NULL);

select * from dbo.contact;

-- -------------------------------------------------------------
-- Data for order table
-- --------------------------------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-22', '2005-04-28', NULL),
(3, '2011-11-23', '2011-12-29', NULL),
(4, '2009-05-19', '2010-06-24', NULL),
(5, '2008-11-23', '2008-12-25', NULL),
(6, '2009-03-15', '2009-11-24', NULL),
(7, '2010-11-21', '2010-11-25', NULL),
(8, '2007-11-20', '2007-09-24', NULL),
(9, '2011-11-18', '2011-12-24', NULL),
(10, '2012-02-12', '2012-05-24', NULL);

select * from dbo.[order];
-- -------------------------------------------------------
-- Data for store table
-- -------------------------------------------------------
INSERT INTO dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
('Walgreens', '1000 Walnut Drive', 'Aspen', 'IL', '475315690','9418887070','info@walgreens.com','www.walgreens.com', NULL),
('cvs', '4400 Chestnut Drive', 'Sarasota', 'FL', '900314699','9418887071','info@cvs.com','www.CVS.com', NULL),
('lowes', '3300 Pecan Drive', 'Tampa', 'FL', '800322680','9418887072','info@lowes.com','www.lowes.com', NULL),
('Walmart', '2200 Cashew Drive', 'Los Angeles', 'CA', '700333670','9418887088','info@walmart.com','www.walmart.com', NULL),
('Fixrepair', '1200 Treenut Drive', 'Los Angeles', 'CA', '705315111','9418887099','info@fixrepair.com','www.fixrepair.com', NULL);

select * from dbo.store;

-- -------------------------------------------------------
-- Data for invoice table
-- -------------------------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2006-11-09', 100.32, 0, NULL),
(1, 1, '2010-12-05', 57.34, 0, NULL),
(3, 2, '2011-10-11', 99.30, 1, NULL),
(2, 3, '2012-06-03', 1109.32, 1, NULL),
(6, 4, '2009-07-12', 239.32, 0, NULL),
(7, 5, '2008-05-02', 587.32, 0, NULL),
(8, 2, '2007-04-02', 699.32, 1, NULL),
(9, 3, '2011-02-03', 934.32, 1, NULL),
(10, 4, '2012-05-04', 27.45.32, 0, NULL);

select * from dbo.invoice;

-- --------------------------------------------------------
-- Data for vendor table
-- --------------------------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '100 Hog Drive', 'Tampa', 'FL', '344761234', '7641238696', 'sales@sysco.com', 'www.sysco.com', NULL),
('ge', '105 Rodent Street', 'Orlando', 'FL', '300761234', '7021438999', 'sales@ge.com', 'www.ge.com', 'Radical store'),
('cisco', '120 Piggy Way', 'Tallahassee', 'FL', '355761222', '8041234666', 'sales@cisco.com', 'www.cisco.com', NULL),
('coolthing', '300 Doggydog Drive', 'Sarasota', 'FL', '344991333', '9951233600', 'sales@coolthing.com', 'www.coolthing.com', 'Coool things here, buy buy buy'),
('Radicaltools', '150 Kittycat Lane', 'Tampa', 'FL', '343261555', '1141232500', 'sales@radicaltools.com', 'www.radicaltools.com', NULL);

select * from dbo.vendor;

-- ------------------------------------------------------------
-- Data for product table
-- ------------------------------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer','', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdirver'),
(2, 'screwdirver','', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail','16 gal', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil','peanut oil', 15, 19, 19.69, 28.99, NULL, 'GALLONS'),
(3, 'frying pan','', 3.5, 45, 4.99, 7.99, 50, 'HALF OFF SALE PRICE');

select * from dbo.product;

-- -------------------------------------------------------------
-- Data for order_line table
-- ------------------------------------------------------------
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(4, 5, 13, 58.99, NULL);

select * from dbo.order_line;

-- -----------------------------------------------------------
-- Data for payment table
-- -----------------------------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, NULL),
(4, '2007-08-12', 6.99, NULL),
(1, '2006-09-01', 12.99, NULL),
(3, '2001-12-11', 10.99, NULL),
(2, '2002-11-10', 51.99, NULL),
(6, '2003-09-04', 50.99, NULL),
(8, '2004-06-03', 54.99, NULL),
(9, '2005-07-01', 99.99, NULL),
(7, '2009-04-02', 10.99, NULL),
(10, '2009-07-01', 13.99, NULL),
(4, '2007-07-01', 4.99, NULL);

select * from dbo.payment;

-- -----------------------------------------------------------
-- Data for product_hist table
-- -----------------------------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased w/ screwdriver set'),
(2, '2009-01-02 12:53:34', 1.99, 3.49, NULL, NULL),
(3, '2008-02-02 11:50:34', 3.99, 7.99, 40, NULL),
(4, '2007-03-02 11:49:34', 19.99, 28.99, NULL, 'GALLONS'),
(5, '2006-05-02 11:17:34', 8.99, 13.99, 50, 'HALF OFF SALE PRICE');

select * from dbo.product_hist;

-- ----------------------------------------------------------
-- Data for srp_hist table
-- ----------------------------------------------------------
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 110000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 110000, 175000, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 110000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);

select * from dbo.srp_hist;

-- get year only from srp_hist
select year(sht_date) from dbo.srp_hist;


-- select * from dbo.person;

-- --------------------------------------------------------------------------------------------------------------------------------- --
-- Populate person table with hashed and salted SSN numbers. MUST include salted value in DB!                                        --
-- --------------------------------------------------------------------------------------------------------------------------------- --
CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

    DECLARE @salt binary(64);
    DECLARE @ran_num int;
    DECLARE @ssn binary(64);
    DECLARE @x INT, @y INT;
    SET @x = 1;

-- dynamically set loop ending value (total number of persons)
    SET @y=(select count(*) from dbo.person);
-- select @y; -- display number of persons (for testing purposes only)

      WHILE (@x <= @y)
      BEGIN
      -- give each person a unique randomized salt, and hashed and salted randomized SSN.
      -- NOTE: this demo is ONLY for showing how to include salted and hashed randomzied values for testing purposes
      -- function returns a cryptographic, randomly-generated hexadecimal number with length of specified nu,ber of bytes
      SET @salt=CRYPT_GEN_RANDOM(64); -- salt includes unique random bytes for each user when looping
      SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive
      SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

      --select @salt, len(@salt), ran_num, len(@ran_num), @ssn, len(@ssn); -- only for testing values

      -- RAND([N]): returns random floating-point value v in the range of 0 <= v < 1.0
      -- Documentation: https://www.technothenet.com/sql_server/functions/rand.php
      -- randomize ssn between 111111111 - 999999999 (Note: using value 000000000 for ex. 4 below)
      update dbo.person
      set per_ssn=@ssn, per_salt=@salt
      where per_id=@x;

      SET @x = @x + 1;

      END;

END;
GO

exec dbo.CreatePersonSSN