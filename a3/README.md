# LIS3781 Advanced Database Management
## Jack H. Miller
**********************************************************************************************************************************************
### LIS3781 A3 Requirements:
- Screenshot of SQL code used to create and populate tables
- Screenshot of populated tables
## Screenshots:
***********************************************************************************************************************************************
### Oracle Customer Table
![Customer Table Creation Statement](imgs/CustomerTableCreationSQL.JPG)
*********************************************************************************************************************************************
### Oracle Commodity Table
![Commodity Table Creation Statement](imgs/CommodityTableCreation.JPG)
*********************************************************************************************************************************************
### Oracle Order Table
![Order Table Creation Statement](imgs/OrderTableCreation.JPG)

*********************************************************************************************************************************************
### ==================================
### Customer Values
![Customer Values](imgs/CustomerTable.JPG)
*********************************************************************************************************************************************
### Commodity Values
![Commodity Values.jpg](imgs/CommodityTable.JPG)
*********************************************************************************************************************************************
### Order Values
![Order Values](imgs/OrderTable.JPG)