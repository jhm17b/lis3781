SET DEFINE OFF

--Customer Table--
DROP SEQUENCE seq_cus_id;
Create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
    cus_id number(3,0) not null, -- max value 999
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null,
    cus_zip number(9) not null, --equivalent to number(9,0)
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2), --maxvalue 9999999.99
    cus_notes varchar2(255),
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);


--Commodity Table--
DROP SEQUENCE seq_com_id; --for auto-increment
Create sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
create table commodity
(
    com_id number not null,
    com_name varchar2(20),
    com_price number(8,2) not null,
    cus_notes varchar2(255),
    CONSTRAINT pk_commodity PRIMARY KEY(com_id),
    CONSTRAINT uq_com_name UNIQUE(com_name) --Case sensitive by default
);

--Order Table--
DROP SEQUENCE seq_ord_id; --for auto-increment
Create sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order"
(
    ord_id number(4,0) not null, --max value 9999 (permitting only integers not decimals)
    cus_id number,
    com_id number,
    ord_num_units number(5,0) not null, --max value 99999 (permitting only integers not decimals)
    ord_total_cost number(8,2) not null,
    ord_notes varchar2(255),
    CONSTRAINT pk_order PRIMARY KEY(ord_id),
    CONSTRAINT fk_order_customer
    FOREIGN KEY (cus_id)
    REFERENCES customer(cus_id),
    CONSTRAINT fk_order_commodity
    FOREIGN KEY (com_id)
    REFERENCES commodity(com_id),
    CONSTRAINT check_unit CHECK(ord_num_units > 0),
    CONSTRAINT check_total CHECK(ord_total_cost > 0)
);

--Oracle NEXTVAL function used to retrieve next value in sequence

--Insert Values for Customer Table--
INSERT INTO customer VALUES (seq_cus_id.nextval,'Steve','Green','123 Main St.','Detroit','MI',40022,9418086060,'sgreen@gmail.com',12400.50,'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval,'George','Green','200 Main St.','Detroit','MI',40022,9418086150,'ggreen@gmail.com',400.50,'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval,'Brenda','Smith','1203 Second St.','Chicago','IL',31022,9005057878,'bsmith@gmail.com',10500.90, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval,'Fred','Smith','1205 Second St.','Chicago','IL',31022,9005057870,'fsmith@gmail.com',7500.00, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval,'Alphonse','Fullmetal','2400 Green St.','San Fransisco','CA',24022,9006068878,'afullmetal@gmail.com',5500.50, NULL);
commit;


--Note: Oracle does not autocommit by default

--Insert Values for Commodity Table--
INSERT INTO commodity VALUES (seq_com_id.nextval,'DVD & Player',109.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval,'Cereal',3.00, 'sugar-free');
INSERT INTO commodity VALUES (seq_com_id.nextval,'Scrabble',29.00, 'Original');
INSERT INTO commodity VALUES (seq_com_id.nextval,'Licorice',1.89, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval,'Tums',2.45, 'antacid');
commit;

--Insert Values for Order Table--
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 50, 200, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 30, 100, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 6, 654, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 24, 972, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 7, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 5, 15, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 40, 57, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 4, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 14, 770, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 15, 883, NULL);
commit;

select * from customer;
select * from commodity;
select * from "order";
