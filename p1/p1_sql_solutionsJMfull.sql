-- ---------------------------------------------------------------------------------------------------------- --
--                                      Create Database and Tables                                            --
-- ---------------------------------------------------------------------------------------------------------- --

-- see data output after insert statement below
select 'drop, create, use database, create tables, display data:' as '';
do sleep(5); -- pause MySQL console output

DROP SCHEMA IF EXISTS jhm17b;
CREATE SCHEMA IF NOT EXISTS jhm17b;
SHOW WARNINGS;
USE jhm17b;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      Person Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

-- Note: allow per_ssn and per_salt to be null, in order to use stored proc CreatePersonSSN below
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL COMMENT '*only* demo purposes - do *NOT* use *salt* in the name',
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    per_email VARCHAR(100) NOT NULL,
    per_dob DATE NOT NULL,
    per_type ENUM('a','c','j') NOT NULL,
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      attorney Table                                                        --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
    per_id SMALLINT UNSIGNED NOT NULL,
    aty_start_date DATE NOT NULL,
    aty_end_date DATE NULL,
    aty_hourly_rate DECIMAL(5,2) NOT NULL,
    aty_years_in_practice TINYINT NOT NULL,
    aty_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_attorney_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      client Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
    per_id SMALLINT UNSIGNED NOT NULL,
    cli_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_client_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      court Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
    crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    crt_name VARCHAR(45) NOT NULL,
    crt_street VARCHAR(30) NOT NULL,
    crt_city VARCHAR(30) NOT NULL,
    crt_state CHAR(2) NOT NULL,
    crt_zip varchar(9) NOT NULL,
    crt_phone BIGINT NOT NULL,
    crt_email VARCHAR(100) NOT NULL,
    crt_url VARCHAR(100) NOT NULL,
    crt_notes VARCHAR(255) NOT NULL,
    PRIMARY KEY (crt_id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      judge Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
    per_id SMALLINT UNSIGNED NOT NULL,
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
    jud_salary DECIMAL(8,2) NOT NULL,
    jud_years_in_practice TINYINT UNSIGNED NOT NULL,
    jud_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    INDEX idx_crt_id (crt_id ASC),

    CONSTRAINT fk_judge_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_judge_court
        FOREIGN KEY (crt_id)
        REFERENCES court (crt_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      judge_hist Table                                                      --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
    jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    jhs_crt_id TINYINT NULL,
    jhs_date timestamp NOT NULL default current_timestamp(),
    jhs_type ENUM('i', 'u', 'd') NOT NULL default 'i',
    jhs_salary DECIMAL(8,2) NOT NULL,
    jhs_notes VARCHAR(255) NULL,
    PRIMARY KEY (jhs_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_judge_hist_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      `case` Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    cse_type VARCHAR(45) NOT NULL,
    cse_description TEXT NOT NULL,
    cse_start_date DATE NOT NULL,
    cse_end_date DATE NULL,
    cse_notes VARCHAR(255) NULL,
    PRIMARY KEY (cse_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_court_case_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Bar Table                                                             --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
    bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    bar_name VARCHAR(45) NOT NULL,
    bar_notes VARCHAR(255) NULL,
    PRIMARY KEY (bar_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_bar_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Specialty Table                                                       --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS speciality;
CREATE TABLE IF NOT EXISTS speciality
(
    spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    spc_type VARCHAR(45) NOT NULL,
    spc_notes VARCHAR(255) NULL,
    PRIMARY KEY (spc_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_speciality_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Assignment Table                                                      --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment
(
    asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_cid SMALLINT UNSIGNED NOT NULL,
    per_aid SMALLINT UNSIGNED NOT NULL,
    cse_id SMALLINT UNSIGNED NOT NULL,
    asn_notes VARCHAR(255) NULL,
    PRIMARY KEY (asn_id),

    INDEX idx_per_cid (per_cid ASC),
    INDEX idx_per_aid (per_aid ASC),
    INDEX idx_cse_id (cse_id ASC),

    UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

    CONSTRAINT fk_assign_case
        FOREIGN KEY (cse_id)
        REFERENCES `case` (cse_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_client
        FOREIGN KEY (per_cid)
        REFERENCES client (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_attorney
        FOREIGN KEY (per_aid)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Phone Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
    phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    phn_num BIGINT UNSIGNED NOT NULL,
    phn_type ENUM('h','c','w','f') NOT NULL COMMENT 'home, cell, work, fax',
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_phone_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;


-- --------------------- --
--    POPULATE TABLES    --
-- --------------------- --

-- per_id values (must match for SQL statements to work)
-- person (super type): 1 - 15
  -- client (sub type): 1-5
  -- attorney (sub type): 6-10
  -- judge (sub type): 11 - 15
  
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN

DECLARE x, y INT;
SET x = 1;

-- Dynamically set loop ending value (total number of persons)
select count(*) into y from person;
-- select y; -- display number of persons (only for testing)

WHILE x <= y DO

    -- give each person randomized salt, and hashed and salted randomized SSN.
    -- Note: this demo is only for showing how to include salted and hashed randomized values for testing purposes
    
    SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping
    SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below)
    SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512)); -- each user's SSN value is uniquely randomized and uniquely salted

    -- RAND([N]): Returns random floating-point value v in the range 0 <= v < 1.0
    -- Documentation: https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_rand
    -- randomize ssn between 111111111 - 999999999 (note: using value 000000000 for ex. 4 below)

    update person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=x;

    SET x = x + 1;

    END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();
-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for person table                                                 --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers','437 Southern Drive','Rochester','NY',324402222,'srogers@gmail.com','1923-10-03','c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne','1000 Moutain Way','Gotham','NY',300403022,'bwayne@gmail.com','1968-03-20','c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker','20 Ingram Street','Queens','NY',100504022,'pparker@gmail.com','1999-03-20','c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark','100 Main Street','New York','NY',100407022,'tstark@gmail.com','1978-05-20','a', NULL),
(NULL, NULL, NULL, 'Bruce', 'Banner','104 Moutain Lane','New York','NY',120503022,'bbanner@gmail.com','1976-02-20','a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pym','300 Second Street','New York','NY',250403022,'hpym@gmail.com','1940-01-15','a', NULL),
(NULL, NULL, NULL, 'Clint', 'Barton','405 Moutain Way','New York','NY',25053022,'cbarton@gmail.com','1977-06-20','c', NULL),
(NULL, NULL, NULL, 'Natasha', 'Brown','607 Moutain Way','New York','NY',25053023,'nbrown@gmail.com','1978-07-13','c', NULL),
(NULL, NULL, NULL, 'Thor', 'Odinson','100 Odin Way','Asgard','NY',99099099,'todinson@gmail.com','1900-07-11','c', NULL),
(NULL, NULL, NULL, 'Clark', 'Kent','301 Prerri Way','New York','NY',30053088,'ckent@gmail.com','1960-05-20','j', NULL),
(NULL, NULL, NULL, 'Ryan', 'Smith','405 Green Way','New York','NY',31053054,'rsmith@gmail.com','1999-03-20','c', NULL),
(NULL, NULL, NULL, 'Steven', 'Miller','4052 Palm Street','Tampa','FL',32033012,'smiller@gmail.com','1967-07-20','j', NULL),
(NULL, NULL, NULL, 'Nick', 'White','4022 Windy Way','Tampa','FL',56056056,'nwhite@gmail.com','1999-07-24','c', NULL),
(NULL, NULL, NULL, 'Drake', 'Smith','3033 Third Street','Tampa','FL',34034034,'dsmith@gmail.com','1975-03-22','a', NULL),
(NULL, NULL, NULL, 'Dave', 'Barton','4022 Second Street','Tampa','FL',12012012,'dbarton@gmail.com','1978-06-19','j', NULL);

COMMIT;
call CreatePersonSSN();

select * from person;