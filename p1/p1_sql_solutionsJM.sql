-- ---------------------------------------------------------------------------------------------------------- --
--                                      Create Database and Tables                                            --
-- ---------------------------------------------------------------------------------------------------------- --

-- see data output after insert statement below
select 'drop, create, use database, create tables, display data:' as '';
do sleep(5); -- pause MySQL console output

DROP SCHEMA IF EXISTS jhm17b;
CREATE SCHEMA IF NOT EXISTS jhm17b;
SHOW WARNINGS;
USE jhm17b;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      Person Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

-- Note: allow per_ssn and per_salt to be null, in order to use stored proc CreatePersonSSN below
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL COMMENT '*only* demo purposes - do *NOT* use *salt* in the name',
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    per_email VARCHAR(100) NOT NULL,
    per_dob DATE NOT NULL,
    per_type ENUM('a','c','j') NOT NULL,
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      attorney Table                                                        --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
    per_id SMALLINT UNSIGNED NOT NULL,
    aty_start_date DATE NOT NULL,
    aty_end_date DATE NULL,
    aty_hourly_rate DECIMAL(5,2) NOT NULL,
    aty_years_in_practice TINYINT NOT NULL,
    aty_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_attorney_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      client Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
    per_id SMALLINT UNSIGNED NOT NULL,
    cli_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_client_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      court Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
    crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    crt_name VARCHAR(45) NOT NULL,
    crt_street VARCHAR(30) NOT NULL,
    crt_city VARCHAR(30) NOT NULL,
    crt_state CHAR(2) NOT NULL,
    crt_zip char(9) UNSIGNED NOT NULL,
    crt_phone BIGINT NOT NULL,
    crt_email VARCHAR(100) NOT NULL,
    crt_url VARCHAR(100) NOT NULL,
    crt_notes VARCHAR(255) NOT NULL,
    PRIMARY KEY (crt_id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      judge Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
    per_id SMALLINT UNSIGNED NOT NULL,
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
    jud_salary DECIMAL(8,2) NOT NULL,
    jud_years_in_practice TINYINT UNSIGNED NOT NULL,
    jud_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    INDEX idx_crt_id (crt_id ASC),

    CONSTRAINT fk_judge_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_judge_court
        FOREIGN KEY (crt_id)
        REFERENCES court (crt_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      judge_hist Table                                                      --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
    jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    jhs_crt_id TINYINT NULL,
    jhs_date timestamp NOT NULL default current_timestamp(),
    jhs_type ENUM('i', 'u', 'd') NOT NULL default 'i',
    jhs_salary DECIMAL(8,2) NOT NULL,
    jhs_notes VARCHAR(255) NULL,
    PRIMARY KEY (jhs_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_judge_hist_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      `case` Table                                                          --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    cse_type VARCHAR(45) NOT NULL,
    cse_description TEXT NOT NULL,
    cse_start_date DATE NOT NULL,
    cse_end_date DATE NULL,
    cse_notes VARCHAR(255) NULL,
    PRIMARY KEY (cse_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_court_case_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Bar Table                                                             --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
    bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    bar_name VARCHAR(45) NOT NULL,
    bar_notes VARCHAR(255) NULL,
    PRIMARY KEY (bar_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_bar_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Specialty Table                                                       --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS speciality;
CREATE TABLE IF NOT EXISTS speciality
(
    spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    spc_type VARCHAR(45) NOT NULL,
    spc_notes VARCHAR(255) NULL,
    PRIMARY KEY (spc_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_speciality_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Assignment Table                                                      --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment
(
    asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_cid SMALLINT UNSIGNED NOT NULL,
    per_aid SMALLINT UNSIGNED NOT NULL,
    cse_id SMALLINT UNSIGNED NOT NULL,
    asn_notes VARCHAR(255) NULL,
    PRIMARY KEY (asn_id),

    INDEX idx_per_cid (per_cid ASC),
    INDEX idx_per_aid (per_aid ASC),
    INDEX idx_cse_id (cse_id ASC),

    UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

    CONSTRAINT fk_assign_case
        FOREIGN KEY (cse_id)
        REFERENCES `case` (cse_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_client
        FOREIGN KEY (per_cid)
        REFERENCES client (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_attorney
        FOREIGN KEY (per_aid)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

-- SHOW WARNINGS;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Phone Table                                                           --
-- ---------------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
    phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    phn_num BIGINT UNSIGNED NOT NULL,
    phn_type ENUM('h','c','w','f') NOT NULL COMMENT 'home, cell, work, fax',
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_phone_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ci;

--SHOW WARNINGS;


-- --------------------- --
--    POPULATE TABLES    --
-- --------------------- --

-- per_id values (must match for SQL statements to work)
-- person (super type): 1 - 15
  -- client (sub type): 1-5
  -- attorney (sub type): 6-10
  -- judge (sub type): 11 - 15


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for person table                                                 --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers','437 Southern Drive','Rochester','NY',324402222,'srogers@gmail.com','1923-10-03','c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne','1000 Moutain Way','Gotham','NY',300403022,'bwayne@gmail.com','1968-03-20','c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker','20 Ingram Street','Queens','NY',100504022,'pparker@gmail.com','1999-03-20','c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark','100 Main Street','New York','NY',100407022,'tstark@gmail.com','1978-05-20','a', NULL),
(NULL, NULL, NULL, 'Bruce', 'Banner','104 Moutain Lane','New York','NY',120503022,'bbanner@gmail.com','1976-02-20','a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pym','300 Second Street','New York','NY',250403022,'hpym@gmail.com','1940-01-15','a', NULL),
(NULL, NULL, NULL, 'Clint', 'Barton','405 Moutain Way','New York','NY',25053022,'cbarton@gmail.com','1977-06-20','c', NULL),
(NULL, NULL, NULL, 'Natasha', 'Brown','607 Moutain Way','New York','NY',25053023,'nbrown@gmail.com','1978-07-13','c', NULL),
(NULL, NULL, NULL, 'Thor', 'Odinson','100 Odin Way','Asgard','NY',99099099,'todinson@gmail.com','1900-07-11','c', NULL),
(NULL, NULL, NULL, 'Clark', 'Kent','301 Prerri Way','New York','NY',30053088,'ckent@gmail.com','1960-05-20','j', NULL),
(NULL, NULL, NULL, 'Ryan', 'Smith','405 Green Way','New York','NY',31053054,'rsmith@gmail.com','1999-03-20','c', NULL),
(NULL, NULL, NULL, 'Steven', 'Miller','4052 Palm Street','Tampa','FL',32033012,'smiller@gmail.com','1967-07-20','j', NULL),
(NULL, NULL, NULL, 'Nick', 'White','4022 Windy Way','Tampa','FL',56056056,'nwhite@gmail.com','1999-07-24','c', NULL),
(NULL, NULL, NULL, 'Drake', 'Smith','3033 Third Street','Tampa','FL',34034034,'dsmith@gmail.com','1975-03-22','a', NULL),
(NULL, NULL, NULL, 'Dave', 'Barton','4022 Second Street','Tampa','FL',12012012,'dbarton@gmail.com','1978-06-19','j', NULL);

COMMIT;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for phone table                                                  --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c', NULL),
(NULL, 2, 2030087707, 'h', NULL),
(NULL, 4, 3040080037, 'w', 'has two office numbers'),
(NULL, 5, 4034400027, 'w', NULL),
(NULL, 6, 4442200030, 'f', 'fax number not currently working'),
(NULL, 7, 9413008899, 'c', 'prefers home calls'),
(NULL, 8, 9412288400, 'h', NULL),
(NULL, 9, 9412271027, 'w', NULL),
(NULL, 10, 8032285533, 'f', 'work fax number'),
(NULL, 11, 8442338827, 'h', 'prefers cell phone calls'),
(NULL, 12, 7732288337, 'w', 'best number to reach'),
(NULL, 13, 7172308844, 'w', 'call during lunch'),
(NULL, 14, 9412230327, 'c', 'prefers cell phone calls'),
(NULL, 15, 4042288123, 'f', 'use for faxing legal docs');

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for client table                                                 --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for attorney table                                               --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-07-12', NULL, 130, 28, NULL),
(8, '2009-05-10', NULL, 70, 17, NULL),
(9, '2007-08-15', NULL, 78, 13, NULL),
(10, '2011-04-11', NULL, 60, 24, NULL);

COMMIT;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for bar table                                                    --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida Bar', NULL),
(NULL, 7, 'Alabama Bar', NULL),
(NULL, 8, 'Georgia Bar', NULL),
(NULL, 9, 'Michigan Bar', NULL),
(NULL, 10, 'South Carolina Bar', NULL),

(NULL, 6, 'Montana Bar', NULL),
(NULL, 7, 'Arizona Bar', NULL),
(NULL, 8, 'Nevada Bar', NULL),
(NULL, 9, 'New York Bar', NULL),
(NULL, 10, 'New York Bar', NULL),

(NULL, 6, 'Mississippi Bar', NULL),
(NULL, 7, 'California Bar', NULL),
(NULL, 8, 'Illinoise Bar', NULL),
(NULL, 9, 'Indiana Bar', NULL),
(NULL, 10, 'Illinoise Bar', NULL),

(NULL, 6, 'Tallahassee Bar', NULL),
(NULL, 7, 'Ocala Bar', NULL),
(NULL, 8, 'Bay County Bar', NULL),
(NULL, 9, 'Cincinati Bar', NULL);

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for specialty table                                              --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES

(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),

(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for court table                                                  --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '100 south monroe street', 'tallahassee', 'fl', 323035292, 8501118080, 'lccc@us.fl.gov', 'https://www.leoncountcircuitcourt.gov/', NULL),
(NULL, 'leon county traffic court', '100 main street', 'tallahassee', 'fl', 323034290, 8502228080, 'lctc@us.fl.gov', 'https://www.leoncounttrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '102 second street', 'tallahassee', 'fl', 323031299, 8503338099, 'fsc@us.fl.gov', 'https://www.floridasupremeccourt.gov/', NULL),
(NULL, 'orange county court house', '105 main street', 'tallahassee', 'fl', 323044288, 8504477091, 'occ@us.fl.gov', 'https://www.ninthcircuit.gov/', NULL),
(NULL, 'fifth district court of appeal', '110 Duval street', 'tallahassee', 'fl', 323124266, 8504455066, '5dca@us.fl.gov', 'https://www.5dca.gov/', NULL);

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for judge table                                                  --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for judge_hist table                                             --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-02-15', 'i', 130000, NULL),
(NULL, 12, 2, '2010-04-11', 'i', 135000, NULL),
(NULL, 13, 5, '2000-05-10', 'i', 140000, NULL),
(NULL, 13, 4, '2005-05-10', 'i', 185000, NULL),
(NULL, 14, 4, '2008-04-12', 'i', 150000, NULL),
(NULL, 15, 1, '2009-02-14', 'i', 165000, 'freshman justice'),
(NULL, 11, 5, '2007-05-11', 'i', 175000, 'assigned to another court'),
(NULL, 12, 4, '2008-04-17', 'i', 190000, 'became chief justice'),
(NULL, 14, 3, '2012-02-16', 'i', 200000, 'reassigned to court based upon local area population growth');

COMMIT;


-- --------------------------------------------------------------------------------------------------------------------------- --
--                                              Data for `case` table                                                          --
-- --------------------------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says that his logo is being used without consent to promote a rival business', '2010-09-09', NULL, 'copyright infringement'),
(NULL, 13, 'criminal', 'client is charged with assaulting her husband', '2009-11-09', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke an ankle while shopping in store', '2009-12-09', '2011-10-20', 'slip and fall'),
(NULL, 11, 'criminal', 'client is charged with assaulting her husband', '2005-12-07', '2007-12-23', 'grand theft'),
(NULL, 13, 'criminal', 'client is charged with possessing 10 grams of cocaine', '2012-12-07', NULL, 'possession of narcotics'),
(NULL, 14, 'civil', 'client alleges newspaper printed false information about himself', '2007-01-07', '2007-05-23', 'defamation'),
(NULL, 12, 'criminal', 'client is charged with the murder of a coworker, no alibe', '2011-01-08', '2012-03-13', 'defamation'),
(NULL, 15, 'civil', 'client does not know the true strength of an IT degree', '2013-01-09', '2014-06-23', 'bankruptcy');

COMMIT;


-- ---------------------------------------------------------------------------------------------------------- --
--                                      Data for assignment table                                             --
-- ---------------------------------------------------------------------------------------------------------- --
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);

COMMIT;

-- ---------------------------------------------------------------------------------------------------------- --
--                                      Securing Data                                                         --
-- ---------------------------------------------------------------------------------------------------------- --

-- Demo sha2() hash function:
-- select sha2('test', 512);

-- length as hex value:
-- select length(sha2('test', 512));

-- sha2() hash function converted to binary
-- select unhex(sha2('test', 512));

-- length as binary:
-- select length(unhex(sha2('test', 512)));

-- Example: hash string "test"
-- SHA2/512 as hexidecimal digits
    -- select SHA2("test", 512) AS 'SHA2/512 Hashed Hexidecimal Digits',
    -- length(unhex(SHA2("test", 512))) AS 'Hexidecimal Byte Count';


-- SHA2/512 as binary bytes (Note: MySQL console cannot display binary data.)
    -- select UNHEX(SHA2("test", 512)) AS 'Binary String',
    -- length(UNHEX(SHA2("test", 512))) AS 'Binary Byte Count';


-- ------------------------------------------------------------------------------------------------- --
--                                      Populate person SSN number                                   --
-- ------------------------------------------------------------------------------------------------- --



-- Populate person table with hashed and salted SSN numbers. *MUST* include salted value in DB!
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN

DECLARE x, y INT;
SET x = 1;

-- Dynamically set loop ending value (total number of persons)
select count(*) into y from person;
-- select y; -- display number of persons (only for testing)

WHILE x <= y DO

    --give each person randomized salt, and hashed and salted randomized SSN.
    --Note: this demo is only for showing how to include salted and hashed randomized values for testing purposes
    
    SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping
    SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below)
    SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512)); -- each user's SSN value is uniquely randomized and uniquely salted

    -- RAND([N]): Returns random floating-point value v in the range 0 <= v < 1.0
    -- Documentation: https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_rand
    -- randomize ssn between 111111111 - 999999999 (note: using value 000000000 for ex. 4 below)

    update person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=x;

    SET x = x + 1;

    END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();

SHOW WARNINGS;

-- show populated per_ssn fields (must use hex() function to convert binary data into readable format)
select 'show populated per_ssn fields after calling stored proc' as '';
select per_id, length(per_ssn) from person order by per_id;
DO SLEEP(7);

DROP PROCEDURE IF EXISTS CreatePersonSSN;

-- ----------------------------------------------------------------------------------------------------- --
--                                      DISPLAY TABLE DATA                                               --
-- ----------------------------------------------------------------------------------------------------- --


-- verify table data: pause 5 seconds for each query result set
select 'person table (auto_increment): per_id 1-15' as '';
select * from person;
DO SLEEP(3);
-- or..
-- select sleep(5); -- though, returns 0

select 'phone table: per_id not all persons have phones (e.g., per_id 3)' as '';
select * from phone;
do sleep(3);

select 'client table: per_id 1-5' as '';
select * from client;
do sleep(3);

select 'attorney table: per_id 6-10' as '';
select * from attorney;
do sleep(3);

select 'speciality table: attorneys per_id 6-10' as '';
select * from speciality;
do sleep(3);

select 'bar table: per_id 6-10' as '';
select * from bar;
do sleep(3);

select 'court table: (auto_increment)' as '';
select * from court;
do sleep(3);

select 'judge table: per_id 11-15, crt_id 1-5' as '';
select * from judge;
do sleep(3);

select 'judge_hist table: per_id 11-15, jhs_crt_id 1-5' as '';
select * from judge_hist;
do sleep(3);

select 'case table: judges per_id 11-15' as '';
select * from `case`;
do sleep(3);

select 'assignment table: per_cid 1-5, per_aid 6-10, cse_id 1-8' as '';
select * from assignment;
do sleep(3);


--Demo: Create error in assignment table above (semicolon instead of comma)
-- select 'Can use exit for debugging-- like this...:' as '';
-- exit

-- ----------------------------------------------------------------------- --
--                              Begin Reports                              --
-- ----------------------------------------------------------------------- --

-- 1.) Create a view that displays attorneys' full names, full addresses, ages, hourly rates,
-- the bar names they've passed, as well as specialties, sort by attorneys' last names.

drop VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS

  select
  concat(per_lname, ", ", per_fname) as name,
  concat(per_street, ", ", per_city, ", ", per_state, " ", per_zip) as address,
  TIMESTAMPDIFF(year, per_dob, now()) as age,
  concat('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
  bar_name, spc_type
  from person
    natural join attorney
    natural join bar
    natural join speciality
    order by per_lname;


select 'display view v_attorney_info' as '';

select * from v_attorney_info;
drop VIEW if exists v_attorney_info;
do sleep(5);


-- 2.) Create a stored proceure that displays how many judges were born in each month of the year, sorted by month.

select 'Step a) Display all persons DOB months: testing MySQL monthname() function' as '';

select per_id, per_fname, per_lname, per_dob, monthname(per_dob) from person;
do sleep(3);

select 'Step b) Display pertinent judge data' as '';

select p.per_id, per_fname, per_lname, per_dob, per_type from person as p natural join judge as j;
do sleep(3);

select 'Final: step c) Stored Proc: Display month numbers, month names, and how many judges were born each month' as '';

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
  select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
  from person
  natural join judge
  group by month_name
  order by month;
END //
DELIMITER ;

select 'calling sp_num_judges_born_by_month()' as '';

CALL sp_num_judges_born_by_month();
do sleep(3);

drop procedure if exists sp_num_judges_born_by_month;


-- 3) Create a stored procedure that displays all case types and descriptions, as well as judges' full names, full addressess,
-- phone numbers, years in practice, for cases they presided over, with their start and end dates, sort by judges' last name

drop procedure if exists sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN

-- check query result sets of associated tables:
/*
select per_id from person;
select * from judge;
select * from phone;
*/

select per_id, cse_id, cse_type, cse_description,
  concat(per_fname, " ", per_lname) as name,
  concat('(', substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
  phn_type,
  jud_years_in_practice,
  cse_start_date,
  cse_end_date
from person
  natural join judge
  natural join `case`
  natural join phone
where per_type='j'
order by per_lname;

END //
DELIMITER ;

select 'calling sp_cases and judges()' as '';

-- 4) Create a trigger that automatically adds a record to the judge history table for every record added to the judge table.

-- Note: technically, judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
-- For person: okay to use NULL for per_ssn. Though, a better solution... binary encrypted SSN

select 'show person data BEFORE adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(3);

-- give person a unique randomized salt, then hashed and salt SSN
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
SET @num=000000000; -- Note: already provided random ssn from 111111111-999999999, inclusive above
SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000

-- add 16th person (a judge), before adding person to judge table
INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, @ssn, @salt, 'Bobby', 'Sue', '100 Main Street', 'Sarasota', 'FL', 324530221, 'bsue@gmail.com', '1950-05-16', 'j', 'new district judge');


select 'show person data AFTER adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(5);


-- 5) Create a trigger that automatically adds a record to the judge history table for every record *modified* in the judge table
select 'show judge/judge_hist data *BEFORE* AFTER INSERT trigger fires (trg_judge_history_after_insert' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- Note: use user(), rather than current_user()
-- current_user() indicates definer of stored routine, view, trigger, or event, not necessarily client user

-- after insert of judge table, insert into judge_hist table
DROP PROCEDURE IF EXISTS trg_judge_history_after_update;
DELIMITER //
CREATE TRIGGER trg_judge_history_after_update
AFTER INSERT ON judge
FOR EACH ROW
BEGIN

INSERT INTO judge_hist
(per_id, jhs_crt_id, jhs_type, jhs_salary, jhs_notes)
VALUES
(NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary, concat("modifying user: ", user(), "Notes: ", NEW.jud_notes));

END //
DELIMITER ;

select 'fire trigger by inserting record into judge table' as '';
do sleep(3);

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
((select count(per_id) from person), 3, 175000, 31, 'transferred from neighboring jurisdicition');

