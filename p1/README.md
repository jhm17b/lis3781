# LIS3781 Advanced Database Management
## Jack H. Miller
**********************************************************************************************************************************************
### Project 1 Overview
As the lead DBA for the local municipality, you are contacted by the city coucnil to design a database in order to track and document the city's court case data.
******************************************
### LIS3781 p1 Business Rules:  
As the lead DBA for a local municipality, you are contacted by the city council to design a database in 
order to track and document the city’s court case data. Some report examples:  
Which attorney is assigned to what case(s)? 
How many unique clients have cases (be sure to add a client to more than one case)? 
How many cases has each attorney been assigned, and names of their clients (return number and 
names)? 
How many cases does each client have with the firm (return a name and number value)? 
Which types of cases does/did each client have/had and their start and end dates? 
Which attorney is associated to which client(s), and to which case(s)? 
Names of three judges with the most number of years in practice, include number of years. 
Also, include the following business rules:  
- An attorney is retained by (or assigned to) one or more clients, for each case. 
- A client has (or is assigned to) one or more attorneys for each case. 
- An attorney has one or more cases. 
- A client has one or more cases. 
- Each court has one or more judges adjudicating. 
- Each judge adjudicates upon exactly one court. 
- Each judge may preside over more than one case. 
- Each case that goes to court is presided over by exactly one judge. 
- A person can have more than one phone number. 
************************************************
### LIS3781 Project 1 Requirements:
- Screenshot of ERD
- Populated Person Table
- Optional: SQL code for required reports
## Screenshots:
***********************************************************************************************************************************************
### ERD Screenshot
![ERD Screenshot](imgs/CourtCaseERD.jpg)
*********************************************************************************************************************************************
### Populated Person Table
![Populated Person Table](imgs/PersonTableData.JPG)
*********************************************************************************************************************************************
### Optional: SQL Code for require Reports
![SQL Code](imgs/sql1.JPG)
### SQL #2
![SQL Code](imgs/sql2.1.JPG)
### sql #2
![SQL Code](imgs/sql2.2.JPG)
### sql #3
![SQL Code](imgs/sql3.JPG)
### sql #4
![SQL Code](imgs/sql4.JPG)
### sql #5
![SQL Code](imgs/sql5.JPG)